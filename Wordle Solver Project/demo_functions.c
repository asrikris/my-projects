// A nice place for you to mess with the functions, while you're developing.

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "search_util.h"

int main(void) {
  char words[10][6] = {"stalk", "scrap", "shear", "batch", "motif",
                       "tense", "ultra", "vital", "ether", "nadir"};

  // make a dynamically-allocated vocabulary that you can mess with, including
  // freeing elements of it when necessary.
  char **vocabulary = calloc(10, sizeof(char *));
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }
  size_t num_words = 10;
  int letter_scores[26];
  char let = 'a';
  for (int i = 0; i < 26; i ++) {
	letter_scores[i] = score_letter(let, vocabulary, num_words);
	let++;
  }
  
  
  printf("Testing score_letter function:\n");
  int num = score_letter('a', vocabulary, num_words);
  if (num == 7) {
          printf("SUCCESS: wanted 7, got %d\n", num);
  } 
  num = score_letter('e', vocabulary, num_words);
  if (num == 3) {
	  printf("SUCCESS: wanted 3, got %d\n", num);
  }
  num = score_letter('s', vocabulary, num_words);
  if (num == 4) {
          printf("SUCCESS: wanted 4, got %d\n", num);
  }
  num = score_letter('t', vocabulary, num_words);
  if (num == 7) {
          printf("SUCCESS: wanted 7, got %d\n", num);
  }

  printf("----------------------------------------\n");


  printf("Testing score_word function:\n");
  num = score_word("stalk", letter_scores);
  if (num == 22) {
	  printf("SUCCESS: wanted 21, got %d\n", num);
  }
  
  num = score_word("motif", letter_scores);
  if (num == 13) {
          printf("SUCCESS: wanted 13, got %d\n", num);
  }

  num = score_word("tense", letter_scores);
  if (num == 16) {
          printf("SUCCESS: wanted 15, got %d\n", num);
  }

  num = score_word("nadir", letter_scores);
  if (num == 18) {
          printf("SUCCESS: wanted 18, got %d\n", num);
  }
  printf("----------------------------------------\n");
  
  printf("Testing filter_vocabulary_gray:\n");

  size_t answer;
  answer = filter_vocabulary_gray('a', vocabulary, num_words);
  if (answer == 7) {
	  printf("SUCCESS: wanted 7, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_gray('r', vocabulary, num_words);
  if (answer == 5) {
          printf("SUCCESS: wanted 5, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }
 
  answer = filter_vocabulary_gray('e', vocabulary, num_words);
  if (answer == 3) {
          printf("SUCCESS: wanted 3, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_gray('k', vocabulary, num_words);
  if (answer == 1) {
          printf("SUCCESS: wanted 1, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  printf("----------------------------------------\n");

  printf("Testing filter_vocabulary_yellow:\n");

  answer = filter_vocabulary_yellow('a', 3, vocabulary, num_words);
  if (answer == 6) {
	  printf("SUCCESS: wanted 6, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_yellow('t', 2, vocabulary, num_words);
  if (answer == 7) {
          printf("SUCCESS: wanted 7, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_yellow('s', 0, vocabulary, num_words);
  if (answer == 9) {
          printf("SUCCESS: wanted 9, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }
  
  answer = filter_vocabulary_yellow('r', 4, vocabulary, num_words);
  if (answer == 8) {
          printf("SUCCESS: wanted 8, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_yellow('l', 1, vocabulary, num_words);
  if (answer == 8) {
          printf("SUCCESS: wanted 8, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }
  printf("----------------------------------------\n");

  printf("Testing filter_vocabulary_green:\n");

  answer = filter_vocabulary_green('a', 3, vocabulary, num_words);
  if (answer == 7) {
          printf("SUCCESS: wanted 7, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_green('t', 2, vocabulary, num_words);
  if (answer == 6) {
          printf("SUCCESS: wanted 6, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_green('s', 0, vocabulary, num_words);
  if (answer == 7) {
          printf("SUCCESS: wanted 7, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_green('e', 4, vocabulary, num_words);
  if (answer == 9) {
          printf("SUCCESS: wanted 9, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }

  answer = filter_vocabulary_green('t', 1, vocabulary, num_words);
  if (answer == 8) {
          printf("SUCCESS: wanted 8, got %zu\n", answer);
  }
  for (int i = 0; i < 10; i++) {
    vocabulary[i] = strdup(words[i]);
  }
  printf("----------------------------------------\n");






  // ... OK we're done, clean up the vocabulary.
  free_vocabulary(vocabulary, num_words);

  return 0;
}
