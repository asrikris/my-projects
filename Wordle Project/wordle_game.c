#include "wordle_lib.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct Letters {
        bool useable;
        char letter;
        char color;
        int position;
};

// Once your other functions are working, please revert your main() to its
// initial state, but please feel free to change it, during development. You'll
// want to test out each individual function!
int main(void) {
  char **vocabulary;
  size_t num_words;
  int num_guesses = 0;
  


  srand(time(NULL));

  // load up the vocabulary and store the number of words in it.
  vocabulary = load_vocabulary("vocabulary.txt", &num_words);

  // Randomly select one of the words from the file to be today's SECRET WORD.
  int word_index = rand() % num_words;
  char *secret = vocabulary[word_index];

  // input buffer -- we'll use this to get a guess from the user.
  char guess[80];

  // buffer for scoring each guess.
  char result[6] = {0};
  bool success = false;
  
  struct Letters letterarray[26];
  char currletter = 'a';
  for (int a = 0; a < 26; a++) {
          letterarray[a].useable = false;
          letterarray[a].letter = currletter;
          currletter++;
          letterarray[a].color = 'b';
          letterarray[a].position = '6';
  }

  printf("time to guess a 5-letter word! (press ctrl-D to exit)\n");
  while (!success) {
    printf("guess: ");
    if (fgets(guess, 80, stdin) == NULL) {
      break;
    }
    // Whatever the user input, cut it off at 5 characters.
    guess[5] = '\0';
         

    if (!valid_guess(guess, vocabulary, num_words)) {
      printf("not a valid guess\n");
      continue;
    } 

    success = score_guess(secret, guess, result);
    if (success) {
	    num_guesses++;
	    printf("%s\n", guess);
            printf("%s\n", result);
	    break;
    }

    bool stop = false;
    for (int g = 0; g < 5 && !stop; g ++) {
	    for (int a = 0; a < 26; a ++) {
		    if (guess[g] == letterarray[a].letter) {
			    if (letterarray[a].color == 'x' || (letterarray[a].color == 'g' && letterarray[a].position != g && letterarray[a].position != 6)) {
				    printf("hey! you can't make that guess, it's inconsistent with what I already told you!\n");
			    	    stop = true;
				    break;
			    }  
		     } 
	    }
    }

    if (stop) {
	continue;
    }
    
    stop = false;    
    for (int a = 0; a < 26; a++) {
	    if (letterarray[a].useable) {
		    int lettercounter = 0;
		    for (int g = 0; g < 5; g ++) {
			    if (letterarray[a].letter == guess[g]) {
				    lettercounter++;
		            }
		    }
	    	    if (lettercounter == 0) {
			    printf("hey! you can't make that guess, it's inconsistent with what I already told you!\n");
			    stop = true;
			    break;
	            }

    	    }
    }
    
    if (stop) {
        continue;
    }


  


    for (int g = 0; g < 5; g ++) {
            for (int a = 0; a < 26; a ++) {
                    if (guess[g] == letterarray[a].letter) {
			    if (result[g] == 'g') {
   			         letterarray[a].useable = true;
            			 letterarray[a].color = 'g';
            			 letterarray[a].position = g;
    		  	    } else if (result[g] == 'y') {
                      		 letterarray[a].useable = true;
            			 letterarray[a].color = 'y';
            			 letterarray[a].position = g;
				 break;
    			    } else if (result[g] == 'x') {
            			 letterarray[a].useable = false;
            			 letterarray[a].color = 'x';
    			    }
		   }
   	   }
    }

    num_guesses++;    
    printf("%s\n", guess);
    printf("%s\n", result);
  }

  if (success) {
    printf("you win, in %d guesses!\n", num_guesses);
  }
  free_vocabulary(vocabulary, num_words);

  return 0;
}
